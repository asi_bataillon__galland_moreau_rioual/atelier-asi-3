package starter.controler;

import java.util.Map;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.util.MultiValueMap;
import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;

import starter.modele.User;
import starter.service.UserService;

@RestController
public class UserControler {

	@Autowired
	private UserService userservice;
	
	@CrossOrigin 
	@RequestMapping(method=RequestMethod.POST,value="/register")
	public User Register(@RequestBody Map<String, String> input) {
		return userservice.Register(input.get("name"),input.get("pseudo"),input.get("psw"));
	}
	
	@CrossOrigin 
	@RequestMapping(method=RequestMethod.GET,value="/userByID")
	public User GetUserByID(@RequestParam("id") int id_user) {
		return userservice.GetUserByID(id_user);
	}

	@CrossOrigin 
	@RequestMapping(method=RequestMethod.POST,value="/getdebited")
	public void GetDebited(@RequestBody Map<String, String> input) {
		userservice.GetDebited(input);
	}

	@CrossOrigin 
	@RequestMapping(method=RequestMethod.POST,value="/getcredited")
	public void GetCredited(@RequestBody Map<String, String> input) {
		userservice.GetCredited(input);
	}

	@CrossOrigin 
    @RequestMapping(method=RequestMethod.POST,value="/UserExists")
    public boolean UserExists(@RequestBody Map<String, String> input) {
        return userservice.UserExists(input.get("pseudo"),input.get("psw"));
    }

    @CrossOrigin 
    @RequestMapping(method=RequestMethod.POST,value="/GetUserByPseudo")
    public User GetUserByPseudo(@RequestBody Map<String, String> input) {
        return userservice.GetUserByPseudo(input.get("pseudo"));
    }

}
