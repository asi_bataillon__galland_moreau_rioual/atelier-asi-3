package starter.service;

import java.util.Map;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import starter.modele.User;
import starter.repository.UserRepository;

@Service
public class UserService {
	
	@Autowired
	UserRepository userRepository;
	
	//Getters of User entity
	public User GetUserByPseudo(String pseudo) {
		User u=userRepository.findUserByPseudo(pseudo);
		return u;
	}
	
	public User GetUserByID(int id) {
		return userRepository.findUserByID(id);
	}

	//Main functions register and login
	public User Register(String name, String pseudo, String psw) {
		if(GetUserByPseudo(pseudo)==null) {
			User u=new User(name,pseudo,psw);
			userRepository.save(u);	
			return u;
		}
		return null;
	}
	
	//Verification of an existing user
	public boolean UserExists(String pseudo,String psw) {
		User u=GetUserByPseudo(pseudo);
		if (u!=null){
			if(u.getPsw().equals(psw)) {
				return true;}
			return false;
		}
		else return false;
	}

	//Action of money of user (used in market)
	public void GetCredited(Map<String,String> input) {
		User u =GetUserByID(Integer.parseInt(input.get("id_seller")));
		int amount=u.getMoney()+Integer.parseInt(input.get("price"));
		userRepository.UpdateMoney(u.getid_user(),amount);
	}
	
	public void GetDebited(Map<String,String> input){
		User u =GetUserByID(Integer.parseInt(input.get("id_buyer")));
		int amount=u.getMoney()-Integer.parseInt(input.get("price"));
		userRepository.UpdateMoney(u.getid_user(),amount);
		}


}

