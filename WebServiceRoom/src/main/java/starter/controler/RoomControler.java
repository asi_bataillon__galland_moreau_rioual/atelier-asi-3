package starter.controler;

import java.util.List;
import java.util.Map;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;

import starter.modele.Card;
import starter.modele.Room;
import starter.service.RoomService;

@RestController
public class RoomControler {
	
	@Autowired
	private RoomService roomservice;
	
	@CrossOrigin
	@RequestMapping(method=RequestMethod.POST,value="/createroom")
	public int createroom(@RequestBody Map<String,String> input) {
		return roomservice.createroom(input);
	}
	
	@CrossOrigin
	@RequestMapping(method=RequestMethod.GET,value="/deleteroom")
	public void deleteroom(@RequestParam("id") int id_room) {
		roomservice.DeleteRoom(id_room);
	}
	
	@CrossOrigin
	@RequestMapping(method=RequestMethod.POST,value="/joinroom")
	public int joinRoom(@RequestBody Map<String,String> input) {
		return roomservice.JoinRoom(Integer.parseInt(input.get("id_card")),Integer.parseInt(input.get("id_room")));
	}
	
	@CrossOrigin
	@RequestMapping(method=RequestMethod.GET,value="/roomaccessible")
	public List<Room> GetRoomAccessible() {
		return roomservice.GetRoomAccessible();
	}
	
	@CrossOrigin
	@RequestMapping(method=RequestMethod.GET,value="/roombyid")
	public Room GetRoomByID(@RequestParam("id") int id_room) {
		return roomservice.getRoomByID(id_room);
	}

	@CrossOrigin
	@RequestMapping(method=RequestMethod.GET,value="/cardinroom")
	public List<Card> GetCardInRoom(@RequestParam("id") int id_room) {
		return roomservice.GetCardInRoom(id_room);
	}
	
}
