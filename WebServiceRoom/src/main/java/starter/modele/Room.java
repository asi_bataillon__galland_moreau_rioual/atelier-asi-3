package starter.modele;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.Table;

@Entity
@Table(name = "rooms")
public class Room {
	
	@Id
	@Column(name = "id_room")
    @GeneratedValue(strategy = GenerationType.AUTO)
	private Integer id_room;
	@Column(name="name")
	private String name;
	@Column(name="carte_proprietaire")
	private int carte_proprietaire;
	@Column(name="carte_adversaire")
	private int carte_adversaire;
	@Column(name="mise")
	int mise;

	public Room() {
		
	}
	
	public Room(String name, int carte_proprietaire,int mise) {
		super();
		this.name = name;
		this.carte_proprietaire = carte_proprietaire;
		this.mise=mise;
		this.carte_adversaire=0;
	}
	public Integer getId_room() {
		return id_room;
	}
	public void setId_room(Integer id_room) {
		this.id_room = id_room;
	}
	public String getName() {
		return name;
	}
	public void setName(String name) {
		this.name = name;
	}
	public void setCarte_proprietaire(int carte_proprietaire) {
		this.carte_proprietaire = carte_proprietaire;
	}
	public int getCard_proprietaire() {
		return this.carte_proprietaire;
	}

	public int getCarte_adversaire() {
		return carte_adversaire;
	}
	public void setCarte_adversaire(int carte_adversaire) {
		this.carte_adversaire = carte_adversaire;
	}
	public void setMiseint(int mise) {
		this.mise=mise;
	}
	public int getMise() {
		return this.mise;
	}
	
}
