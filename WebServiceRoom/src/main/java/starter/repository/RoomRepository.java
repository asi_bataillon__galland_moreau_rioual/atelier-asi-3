package starter.repository;

import java.util.List;

import org.springframework.data.jpa.repository.Modifying;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.CrudRepository;
import org.springframework.transaction.annotation.Transactional;

import starter.modele.Card;
import starter.modele.Room;

public interface RoomRepository extends CrudRepository<Room, Integer> {
	
    @Query("select r from Room r where r.id_room=?1")
	public Room getRoomById(int id_room);

    @Query("SELECT MAX(id_room) FROM Room")
	public int getMaxid();
    
    @Transactional
    @Modifying
    @Query("update Room set carte_adversaire=:carte_adversaire where id_room=:id_room ")
    public void joinroom(int carte_adversaire,int id_room);
    
    @Query("select r from Room r where r.carte_adversaire=0")
	public List<Room> getRoomAccessible();

    @Query("select carte_proprietaire from Room r where r.id_room=:id_room ")
	public Integer getcard_proprietaire(int id_room);
    
    @Query("select carte_adversaire from Room r where r.id_room=:id_room ")
	public Integer getcard_adversaire(int id_room);

}
