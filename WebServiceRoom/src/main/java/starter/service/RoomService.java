package starter.service;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpEntity;
import org.springframework.http.HttpHeaders;
import org.springframework.http.MediaType;
import org.springframework.stereotype.Service;
import org.springframework.web.client.RestTemplate;

import starter.modele.Card;
import starter.modele.Room;
import starter.modele.User;
import starter.repository.RoomRepository;

@Service
public class RoomService {
	
	RestTemplate restTemplate = new RestTemplate();
	
	@Autowired
	RoomRepository roomRepository;


	public int createroom(Map<String, String> input) {
		int id_creator=Integer.parseInt(input.get("id_creator"));
		User u_creator = restTemplate.getForObject("http://localhost:8082/userByID" + "?id="+Integer.toString(id_creator), User.class);
		int mise=Integer.parseInt(input.get("mise"));
		if(u_creator.getMoney()>=mise){
			Map<String, String> map = new HashMap<>();
			map.put("price", Integer.toString(mise));
			map.put("id_buyer", Integer.toString(id_creator));
			HttpHeaders headers = new HttpHeaders();
		    headers.setAccept(Arrays.asList(MediaType.APPLICATION_JSON));
			headers.setContentType(MediaType.APPLICATION_JSON);
			HttpEntity<Map<String, String>> entity = new HttpEntity<>(map, headers);
			restTemplate.postForEntity("http://localhost:8082/getdebited",  entity, void.class);
			Room r=new Room(input.get("name"), Integer.parseInt(input.get("carte_proprietaire")),mise);
			roomRepository.save(r);
			return roomRepository.getMaxid();
		}
		return 0;
	}
		
	public void DeleteRoom(int id_room) {
		Room R=getRoomByID(id_room);
		roomRepository.delete(R);
	}

	public int JoinRoom(int id_card, int id_room) {
		Integer id_user=restTemplate.getForObject("http://localhost:8081/getowner" + "?id="+Integer.toString(id_card), Integer.class);
		User u_joiner = restTemplate.getForObject("http://localhost:8082/userByID" + "?id="+Integer.toString(id_user), User.class);
		Room r=getRoomByID(id_room);
		if(u_joiner.getMoney()<r.getMise()) {
			return 0;
		}
		roomRepository.joinroom(id_card, id_room);
		return r.getId_room();
	}

	public Room getRoomByID(int id_room) {
		return roomRepository.getRoomById(id_room);
	}

	public List<Room> GetRoomAccessible() {
		return roomRepository.getRoomAccessible();
	}

	public List<Card> GetCardInRoom(int id_room) {
		List<Card> cards=new ArrayList<Card>();
		Integer int_card_proprietaire=roomRepository.getcard_proprietaire(id_room);
		Integer int_card_adversaire=roomRepository.getcard_adversaire(id_room);
		cards.add(restTemplate.getForObject("http://localhost:8081/getcardbyid" + "?id="+Integer.toString(int_card_proprietaire), Card.class));
		cards.add(restTemplate.getForObject("http://localhost:8081/getcardbyid" + "?id="+Integer.toString(int_card_adversaire), Card.class));

		return cards;
	}

	

}
