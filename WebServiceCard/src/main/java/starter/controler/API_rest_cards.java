package starter.controler;

import java.util.List;
import java.util.Map;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;

import starter.modele.Card;
import starter.modele.CardInstance;
import starter.service.CardService;

@RestController
public class API_rest_cards {

	@Autowired
	private CardService cardservice;
	
	@CrossOrigin 
	@RequestMapping(method=RequestMethod.GET,value="/getcard")
	public List<CardInstance> GetCardInstancesByUser(@RequestParam("id") int id_user) {
		return cardservice.GetCardsByUser(id_user);
	}

	@CrossOrigin 
	@RequestMapping(method=RequestMethod.GET,value="/getcardbyid")
	public Card GetCardInfosByID(@RequestParam("id") int id_card) {
		System.out.println(cardservice.GetCardInfosByID(id_card));return cardservice.GetCardInfosByID(id_card);
	}
	
	@CrossOrigin 
	@RequestMapping(method=RequestMethod.POST,value="/addcardinstances")
	public void AddCardInstance(@RequestBody Map<String, String> entry){
		cardservice.AddCardInstance(entry);
	}

	@CrossOrigin
	@RequestMapping(method=RequestMethod.POST,value="/addcardinfos")
	public void AddCardInfos(@RequestBody Map<String, String> entry){
		cardservice.AddCardInfos(entry);
	}


	@CrossOrigin
	@RequestMapping(method=RequestMethod.GET,value="/GetCardInMarket")
	public List<CardInstance> GetCardInMarket(){
		return cardservice.GetCardMarket();
	}
	
	@CrossOrigin 
	@RequestMapping(method=RequestMethod.GET,value="/changemarket")
	public void ChangeMarketSituation(@RequestParam("id") int id_card){
		cardservice.ChangeMarketSituation(id_card);
	}
	
	@CrossOrigin 
	@RequestMapping(method=RequestMethod.POST,value="/changeowner")
	public void ChangeOwner(@RequestBody Map<String, String> input){
		cardservice.changeowner(input);
	}

}
