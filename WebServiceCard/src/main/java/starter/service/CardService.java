package starter.service;

import java.util.List;
import java.util.Map;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import starter.repository.CardInfosRepository;
import starter.repository.CardInstancesRepository;

import starter.modele.Card;
import starter.modele.CardInstance;
import starter.modele.FAMILY;
import com.google.api.client.http.HttpRequest;
import com.google.api.client.http.HttpRequestFactory;
import com.google.api.client.http.javanet.NetHttpTransport;


@Service
public class CardService {

	HttpRequestFactory requestFactory
    = new NetHttpTransport().createRequestFactory();
    HttpRequest request;
	
	@Autowired
	 CardInfosRepository cardInfosRepository;
	@Autowired
	CardInstancesRepository cardInstancesRepository;


	public CardInstance GetCardInstanceByID(int id_card) {
		return cardInstancesRepository.GetCardByID(id_card);
	}

	public Card GetCardInfosByID(int id_card) {
		return cardInfosRepository.GetCardByID(id_card);
	}

	public List<CardInstance> GetCardsByUser(int id_user) {
		return cardInstancesRepository.GetCardsByUser(id_user);
	}
	public List<CardInstance> GetCardMarket() {
		return cardInstancesRepository.findAllmarket();
	}
	public void newPlayer(int id_player){
		CardInstance salameche=new CardInstance(id_player, 2);
		CardInstance bulbizard=new CardInstance(id_player, 0);
		CardInstance carapuce=new CardInstance(id_player, 1);

		cardInstancesRepository.save(bulbizard);
		cardInstancesRepository.save(carapuce);
		cardInstancesRepository.save(salameche);
	}
	public void AddCardInfos(Map<String,String> entry) {

		Card c=new Card(/*0, */entry.get("name"), entry.get("description"), FAMILY.valueOf(entry.get("family_name")), entry.get("imgurl"),Integer.parseInt(entry.get("hp")),Integer.parseInt(entry.get("attack"))
				, Integer.parseInt(entry.get("defence")));
		cardInfosRepository.save(c);
	}
	public void AddCardInstance(Map<String,String> entry) {

		CardInstance c=new CardInstance(Integer.parseInt(entry.get("owner_id")), Integer.parseInt(entry.get("card_id_reference")));
		cardInstancesRepository.save(c);
	}
	public void ChangeMarketSituation(int id_card) {
		CardInstance C=cardInstancesRepository.GetCardByID(id_card);
		if(C.getFlagmarket()) {
			cardInstancesRepository.ChangeMarketSituation(false,id_card);}
		else 
			cardInstancesRepository.ChangeMarketSituation(true,id_card);
		
	}
	
	public void changeowner(Map<String, String> input) {
		cardInstancesRepository.Changeowner(Integer.parseInt(input.get("id_card")),
				Integer.parseInt(input.get("id_buyer")));
	}

}