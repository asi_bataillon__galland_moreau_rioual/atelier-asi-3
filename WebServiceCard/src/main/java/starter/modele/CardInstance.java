package starter.modele;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.Table;

@Entity
@Table(name = "CardInstances")
public class CardInstance {
	@Id
	@Column(name = "id_card")
    @GeneratedValue(strategy = GenerationType.AUTO)
	private Integer id_card;
	@Column(name="owner_id")
	private int owner_id;
	@Column(name="card_id")
	private int card_id_reference;

	@Column(name="flagmarket")
	private Boolean flagmarket;

	

	
	public CardInstance(int owner_id, int card_id_reference) {
		super();
		this.owner_id = owner_id;
		this.card_id_reference=card_id_reference;
		this.flagmarket=false;
	}

	public Integer getId_card_instance() {
		return id_card;
	}

	public void setId_card_instance(Integer id_card_instance) {
		this.id_card = id_card_instance;
	}

	public void setOwner_id(int owner_id) {
		this.owner_id = owner_id;
	}

	public void setCard_id_reference(int card_id_reference) {
		this.card_id_reference = card_id_reference;
	}

	public void setFlagmarket(Boolean flagmarket) {
		this.flagmarket = flagmarket;
	}

	public int getOwner_id() {
		return owner_id;
	}

	public int getCard_id_reference() {
		return card_id_reference;
	}

	public Boolean getFlagmarket() {
		return flagmarket;
	}
}
