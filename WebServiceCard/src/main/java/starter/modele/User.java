package starter.modele;

import java.util.Collection;

import javax.persistence.CascadeType;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.OneToMany;
import javax.persistence.Table;

import com.fasterxml.jackson.annotation.JsonManagedReference;

@SuppressWarnings("unused")
public class User {

    private int id_user;
	
	static int id_userstatic=0;
	
	private String pseudo;
	
	private String name;
	
	private String psw;
	
	private int money;
	
    public User() {
    	
    }
    
	public User(String name, String pseudo, String psw) {
		super();
		this.id_user = id_userstatic;
		id_userstatic++;
		this.name=name;
		this.pseudo = pseudo;
		this.psw = psw;
		this.money = 5000;
	}

	public int getid_user() {
		return id_user;
	}

	public void setid_user(int id_user) {
		this.id_user = id_user;
	}

	public String getPseudo() {
		return pseudo;
	}

	public void setPseudo(String pseudo) {
		this.pseudo = pseudo;
	}

	public String getPsw() {
		return psw;
	}

	public String getName() {
		return name;
	}

	public void setName(String name) {
		this.name = name;
	}

	public void setPsw(String psw) {
		this.psw = psw;
	}

	public int getMoney() {
		return money;
	}

	public void setMoney(int money) {
		this.money = money;
	}


}
