package starter.modele;

import org.hibernate.annotations.GenericGenerator;

import javax.persistence.*;

@Entity
@Table(name = "Cards")
public class Card {
    @Id
    @Column(name = "id")
    @GeneratedValue(strategy = GenerationType.AUTO, generator="seq")
    @GenericGenerator(name = "seq", strategy="increment")
    private Integer id;

    @Column(name="name")
    private String name;

    public Integer getId() {
        return id;
    }

    public void setId(Integer id) {
        this.id = id;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getDescription() {
        return description;
    }

    public void setDescription(String description) {
        this.description = description;
    }

    public FAMILY getFamily() {
        return family;
    }

    public void setFamily(FAMILY family) {
        this.family = family;
    }

    public String getImgurl() {
        return imgurl;
    }

    public void setImgurl(String imgurl) {
        this.imgurl = imgurl;
    }

    public int getHp() {
        return hp;
    }

    public void setHp(int hp) {
        this.hp = hp;
    }

    public int getAttack() {
        return attack;
    }

    public void setAttack(int attack) {
        this.attack = attack;
    }

    public int getDefence() {
        return defence;
    }

    public void setDefence(int defence) {
        this.defence = defence;
    }

    @Override
    public String toString() {
        return "Card{" +
                "id=" + id +
                ", name='" + name + '\'' +
                ", description='" + description + '\'' +
                ", family=" + family +
                ", imgurl='" + imgurl + '\'' +
                ", hp=" + hp +
                ", attack=" + attack +
                ", defence=" + defence +
                '}';
    }

    @Column(name="description")
    private String description;

    @Column(name="family")
    @Enumerated(EnumType.STRING)
    private FAMILY family;

    @Column(name="imgurl")
    private String imgurl;

    @Column(name="hp")
    private int hp;
    @Column(name="attack")
    private int attack;
    @Column(name="defence")
    private int defence;
    public Card(String name,String description, FAMILY family, String imgurl, int hp, int attack, int defence) {
        this.name=name;
        this.description = description;
        this.family = family;
        this.imgurl = imgurl;
        this.hp = hp;
        this.attack = attack;
        this.defence = defence;
    }

    public Card() {
    }
}
