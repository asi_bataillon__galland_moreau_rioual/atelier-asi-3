package starter.modele;
enum FamilyName{
    EAU, FEU, PLANTE, TERRE
}
public enum FAMILY {

    EAU(FamilyName.EAU,"file:///home/matthieu/Images/eau.png",FamilyName.TERRE),

    FEU(FamilyName.FEU, "file:///home/matthieu/Images/feu.png", FamilyName.EAU),

    TERRE(FamilyName.TERRE, "file:///home/matthieu/Images/terre.png", FamilyName.PLANTE),
    PLANTE(FamilyName.PLANTE, "file:///home/matthieu/Images/plante.png",FamilyName.FEU);
    private FamilyName name;
    private String url_img;
    private FamilyName weakness;
    private FAMILY(FamilyName name, String url_img, FamilyName weakness){
        this.name=name;
        this.url_img=url_img;
        this.weakness=weakness;
    }
}
