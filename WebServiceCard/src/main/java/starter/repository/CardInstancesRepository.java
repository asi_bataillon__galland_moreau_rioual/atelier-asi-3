package starter.repository;

import org.springframework.data.jpa.repository.Modifying;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.CrudRepository;
import org.springframework.stereotype.Repository;
import org.springframework.transaction.annotation.Transactional;
import starter.modele.CardInstance;

import java.util.List;

@Repository
public interface CardInstancesRepository extends CrudRepository<CardInstance, Integer> {

    @Query("select c from CardInstance c where c.owner_id=?1")
    public List<CardInstance> GetCardsByUser(int id_user);

    @Query("select c from CardInstance c where c.id_card=?1")
    public CardInstance GetCardByID(int id_card);

    @Query("SELECT c from CardInstance c where c.flagmarket=true")
    public List<CardInstance> findAllmarket();

    @Transactional
    @Modifying
    @Query("UPDATE CardInstance SET flagmarket=:b WHERE id_card=:id_card")
    public void ChangeMarketSituation(Boolean b,int id_card);

    @Transactional
    @Modifying
    @Query("UPDATE CardInstance SET owner_id=:user_id WHERE id_card=:id_card")
    public void Changeowner(int id_card, int user_id);

    @Query("SELECT owner_id from CardInstance c where c.id_card=:id_card")
    public int getowner(int id_card);

}