package starter.repository;

import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.CrudRepository;

import org.springframework.stereotype.Repository;
import starter.modele.Card;

@Repository
public interface CardInfosRepository extends CrudRepository<Card, Integer> {

    @Query("select c from Card c where c.id=?1")
	public Card GetCardByID(int id_card);
	
}