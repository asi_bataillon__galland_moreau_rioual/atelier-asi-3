package Starter;


import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

import Starter.MarketApiApp;

@SpringBootApplication
public class MarketApiApp {

	public static void main(String[] args) {
        SpringApplication.run(MarketApiApp.class, args);

    }

}
