package Starter.service;

import java.util.Arrays;
import java.util.HashMap;
import java.util.Map;

import org.springframework.http.HttpEntity;
import org.springframework.http.HttpHeaders;
import org.springframework.http.HttpMethod;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Service;
import org.springframework.util.LinkedMultiValueMap;
import org.springframework.util.MultiValueMap;
import org.springframework.web.client.RestTemplate;


import Starter.modele.Card;
import Starter.modele.User;

@Service
public class MarketService {
	
	RestTemplate restTemplate = new RestTemplate();

	
	@SuppressWarnings("unused")
	public boolean SellCard(int id_card) {
		Card c = restTemplate.getForObject("http://localhost:8081/getcardbyid "+ "?id="+Integer.toString(id_card), Card.class);
		if(c.getFlagmarket()==1) {
			return false;
		}
		else {
			ResponseEntity<String> response= restTemplate.getForEntity("http://localhost:8081/changemarket" + "?id="+Integer.toString(id_card), String.class);
			return true;
		}
	}
	
	@SuppressWarnings("unused")
	public boolean BuyCard(int id_card,int id_buyer) {
		User u_buyer = restTemplate.getForObject("http://localhost:8082/userByID" + "?id="+Integer.toString(id_buyer), User.class);
		Card c = restTemplate.getForObject("http://localhost:8081/getcardbyid"  + "?id="+Integer.toString(id_card), Card.class);
		int id_seller=c.getOwner();
		int price=c.getPrice();


		if(u_buyer.getMoney()>price) {
			Map<String, String> map = new HashMap<>();
			map.put("price", Integer.toString(price));
			map.put("id_buyer", Integer.toString(id_buyer));
			map.put("id_seller", Integer.toString(id_seller));
			map.put("id_card", Integer.toString(id_card));

			HttpHeaders headers = new HttpHeaders();
		    headers.setAccept(Arrays.asList(MediaType.APPLICATION_JSON));
			headers.setContentType(MediaType.APPLICATION_JSON);
			HttpEntity<Map<String, String>> entity = new HttpEntity<>(map, headers);
			ResponseEntity<Void> response = restTemplate.postForEntity("http://localhost:8082/getdebited",  entity, void.class);			
			ResponseEntity<Void> response2 = restTemplate.postForEntity("http://localhost:8082/getcredited",  entity, void.class);
			ResponseEntity<Void> response3= restTemplate.postForEntity("http://localhost:8081/changeowner",entity,void.class);
			ResponseEntity<String> response4= restTemplate.getForEntity("http://localhost:8081/changemarket" + "?id="+Integer.toString(id_card), String.class);

			return true;}
		else return false;
	}

}
