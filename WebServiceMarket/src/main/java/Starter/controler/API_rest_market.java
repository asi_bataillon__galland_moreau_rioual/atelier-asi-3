package Starter.controler;


import java.util.Map;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;

import Starter.service.MarketService;

@RestController
public class API_rest_market {

	@Autowired
	private MarketService marketservice;
	

	@CrossOrigin
	@RequestMapping(method=RequestMethod.GET,value="/sellcard")
	public boolean SellCard(@RequestParam("id") int id_card) {
		System.out.println(id_card);
		return marketservice.SellCard(id_card);
	}
	
	@CrossOrigin
	@RequestMapping(method=RequestMethod.POST,value="/buycard")
	public boolean AchatCard(@RequestBody Map<String,String> input) {
		return marketservice.BuyCard(Integer.parseInt(input.get("id_card")),Integer.parseInt(input.get("id_buyer")));
	}

}
