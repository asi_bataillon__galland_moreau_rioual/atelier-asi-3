
function Requete(data, URL, method) {
    var result="";
    $.ajax({
      url:URL,
      type:method,
      contentType : "application/json",
      data :  JSON.stringify(data),
      async: false,
      success:function(data) {
         result = data; 
      }
   });
   return result;
};

function attendre(){
    id = sessionStorage.getItem('id_room');
    url = "http://localhost:8085/roombyid?id="+id;
    var retour_requete = Requete("", url, "GET");
    if (retour_requete.carte_adversaire){
        window.location.href = "./playRoom.html"
    }
    setTimeout(attendre,5000)
}
attendre()

function initialisation() {
    if(typeof sessionStorage!='undefined') {
        var nom = sessionStorage.getItem('name');
        $("#userNameId").html(nom);
        var solde = sessionStorage.getItem('money');
        $("#solde").html(solde);
        var roomNameId = sessionStorage.getItem('nom_de_la_room');
        $("#roomNameId").html(roomNameId);
    }
}