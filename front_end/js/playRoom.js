$(document ).ready(function(){

$('#actionTextId')[0].innerText="jocker attack for 5 \n Superman attack for 41, \n Jocker miss attack";

});

function Requete(data, URL, method) {
  var result="";
  $.ajax({
    url:URL,
    type:method,
    contentType : "application/json",
    data :  JSON.stringify(data),
    async: false,
    success:function(data) {
       result = data; 
    }
 });
 return result;
};

function initialisation() {
  if(typeof sessionStorage!='undefined') {
      var nom = sessionStorage.getItem('name');
      $("#userNameId").html(nom);
      var solde = sessionStorage.getItem('money');
      $("#solde").html(solde);


      //INFOS ROOM
      id = sessionStorage.getItem('id_room');
      url = "http://localhost:8085/roombyid?id="+id;
      var retour_requete = Requete("", url, "GET");

      $("#roomNameId").html(retour_requete.name);


      //INFOS USER ADVERSAIRE
      var id_card = sessionStorage.getItem('id_carte_adversaire');
      url_carte = "http://localhost:8081/getowner?id="+id_card;
      var retour_requete_carte = Requete("", url_carte, "GET");
      var id_user = retour_requete_carte.owner_id;
      url_user = "http://localhost:8082/userByID?id="+id_user;
      var retour_requete_user = Requete("", url_user, "GET");
      var nom_adversaire = retour_requete_user.name;

      $("#proprietaireNameId").html(nom);
      $("#adversaireNameId").html(nom_adversaire);

  }
}


function fillCard(prefix,imgUrlFamily,familyName,imgUrl,name,description,hp,energy,attack,defence,price){
  //FILL THE CURRENT CARD
  $('#'+prefix+'cardFamilyImgId')[0].src=imgUrlFamily;
  $('#'+prefix+'cardFamilyNameId')[0].innerText=familyName;
  $('#'+prefix+'cardImgId')[0].src=imgUrl;
  $('#'+prefix+'cardNameId')[0].innerText=name;
  $('#'+prefix+'cardDescriptionId')[0].innerText=description;
  $('#'+prefix+'cardHPId')[0].innerText=hp+" HP";
  $('#'+prefix+'cardEnergyId')[0].innerText=energy+" Energy";
  $('#'+prefix+'cardAttackId')[0].innerText=attack+" Attack";
  $('#'+prefix+'cardDefenceId')[0].innerText=defence+" Defence";

};

function affichage_carte(id,user){
  url = "http://localhost:8081/getcardbyid?id=" + id;
  var retour_requete = Requete("", url, "GET");

      fillCard(user, retour_requete.urlfamily,retour_requete.familyName,retour_requete.imgurl,retour_requete.name,retour_requete.description,retour_requete.hp,retour_requete.energy,retour_requete.attack,retour_requete.defence,retour_requete.price);
};

var user1 = "user1";
var user2 = "user2";

id = sessionStorage.getItem('id_room');
url = "http://localhost:8085/roombyid?id="+id;
var retour_requete = Requete("", url, "GET");

carteA = retour_requete.carte_adversaire;
carteP = retour_requete.card_proprietaire;

affichage_carte(carteA,user2);
affichage_carte(carteP,user1);


