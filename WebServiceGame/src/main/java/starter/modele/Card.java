package starter.modele;

public class Card {

	private Integer id_card;
	private String name;
	private String description;
	private String familyName;
	private String urlfamily;
	private String imgurl;
	private int hp;
	private int energy;
	private int attack;
	private int defence;
	private int price;
	private int flagmarket;
	private int owner_id;
	

	
	public Card(Integer id_card, String name, String description, String familyName, String urlfamily, String imgurl,
			int hp, int energy, int attack, int defence, int price, int owner_id) {
		super();
		this.id_card = id_card;
		this.name = name;
		this.description = description;
		this.familyName = familyName;
		this.urlfamily = urlfamily;
		this.imgurl = imgurl;
		this.hp = hp;
		this.energy = energy;
		this.attack = attack;
		this.defence = defence;
		this.price = price;
		this.owner_id = owner_id;
		this.flagmarket=0;
	}
	

	public int getPrice() {
		return price;
	}

	public void setPrice(int price) {
		this.price = price;
	}


	
	public String getFamilyName() {
		return familyName;
	}

	public void setFamilyName(String familyName) {
		this.familyName = familyName;
	}

	public String getUrlfamily() {
		return urlfamily;
	}

	public void setUrlfamily(String urlfamily) {
		this.urlfamily = urlfamily;
	}

	public int getEnergy() {
		return energy;
	}

	public void setEnergy(int energy) {
		this.energy = energy;
	}

	public int getAttack() {
		return attack;
	}

	public void setAttack(int attack) {
		this.attack = attack;
	}

	public int getDefence() {
		return defence;
	}

	public void setDefence(int defence) {
		this.defence = defence;
	}

	public Card() {
		
	}

	public Card(int id, String name2, String description2, String familyName2, String urlfamily2, String imgurl2,
			int hp, int energy, int attack, int defence, int price, int owner_id) {
		super();
		this.id_card = id;
		this.name = name2;
		this.description = description2;
		this.familyName = familyName2;
		this.urlfamily = urlfamily2;
		this.imgurl = imgurl2;
		this.hp = hp;
		this.energy = energy;
		this.attack = attack;
		this.defence = defence;
		this.price = price;
		this.owner_id = owner_id;
		this.flagmarket=0;
		}



	public int getId() {
		return id_card;
	}
	public int getOwner() {
		return owner_id;
	}

	public void setOwner(int owner_id) {
		this.owner_id = owner_id;
	}

	public void setId(int id) {
		this.id_card = id;
	}
	
	public String getName() {
		return name;
	}
	public void setName(String name) {
		this.name = name;
	}
	
	public String getDescription() {
		return description;
	}
	public void setDescription(String description) {
		this.description = description;
	}
	
	public String getImgurl() {
		return imgurl;
	}
	public void setImgurl(String imgurl) {
		this.imgurl = imgurl;
	}

	public int getHp() {
		return hp;
	}

	public void setHp(int hp) {
		this.hp = hp;
	}
	
	public int getFlagmarket() {
		return flagmarket;
	}


	public void setFlagmarket(int flagmarket) {
		this.flagmarket = flagmarket;
	}


}