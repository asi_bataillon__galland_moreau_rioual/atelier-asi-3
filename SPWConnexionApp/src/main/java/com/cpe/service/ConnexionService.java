package com.cpe.service;

import java.util.Arrays;
import java.util.HashMap;
import java.util.Map;

import org.springframework.http.HttpEntity;
import org.springframework.http.HttpHeaders;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Service;
import org.springframework.web.client.RestTemplate;

import com.cpe.modele.User;

@Service
public class ConnexionService {
	
	RestTemplate restTemplate = new RestTemplate();

	public User Login(String pseudo,String psw) {
		Map<String, String> map = new HashMap<>();
		map.put("pseudo", pseudo);
		map.put("psw", psw);
		HttpHeaders headers = new HttpHeaders();
	    headers.setAccept(Arrays.asList(MediaType.APPLICATION_JSON));
		headers.setContentType(MediaType.APPLICATION_JSON);
		HttpEntity<Map<String, String>> entity = new HttpEntity<>(map, headers);
		ResponseEntity<Boolean> response = restTemplate.postForEntity("http://localhost:8082/UserExists",  entity, Boolean.class);

		
		if(response.getBody()) {
			Map<String, String> map_user = new HashMap<>();
			map_user.put("pseudo", pseudo);
			HttpHeaders headers_user = new HttpHeaders();
		    headers_user.setAccept(Arrays.asList(MediaType.APPLICATION_JSON));
			headers_user.setContentType(MediaType.APPLICATION_JSON);
			HttpEntity<Map<String, String>> entity_user = new HttpEntity<>(map_user, headers_user);
			User response_user = restTemplate.postForObject("http://localhost:8082/GetUserByPseudo",  entity_user, User.class);
			return (response_user);
		}
		else {
			return null;
		}


	}


}


