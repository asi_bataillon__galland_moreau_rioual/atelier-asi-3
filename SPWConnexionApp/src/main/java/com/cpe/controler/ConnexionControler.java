package com.cpe.controler;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RestController;

import com.cpe.modele.User;
import com.cpe.service.ConnexionService;
import java.util.Map;
@RestController
public class ConnexionControler {
	@Autowired
	private ConnexionService connexionservice;
	
	@CrossOrigin 
	@RequestMapping(method=RequestMethod.POST,value="/connexion")
	public User connection(@RequestBody Map<String, String> payload) {
		return connexionservice.Login(payload.get("pseudo"),payload.get("psw"));
	}
}


